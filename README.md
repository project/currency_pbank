CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides Currency with currency exchange rates through
Privatbank.ua, which are updated continually every day.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/currency_pbank

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/currency_pbank

Works only with UAH currency.

Possible exchange rates:

 * UAH <-> USD
 * UAH <-> EUR
 * UAH <-> RUR


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Currency PrivatBank Rates module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Regional and language >
       Currency exchange > Privatbank exchange rates > Privatbank exchange
       rates > Add an exchange rate.


MAINTAINERS
-----------

 * Vasiliy Zaytsev (vaza18) - https://www.drupal.org/u/vaza18

Supporting organizations:

 * FFW Agency - https://www.drupal.org/ffw-agency
