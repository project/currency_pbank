<?php

namespace Drupal\currency_pbank\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\currency\FormHelperInterface;
use Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the configuration form for the currency_pbank_rates plugin.
 */
class PbankRatesForm extends FormBase implements ContainerInjectionInterface {

  const P24_API_AVAILABLE_CURRENCIES = 'USD,EUR,RUB';

  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * The currency exchange rate provider manager.
   *
   * @var \Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface
   */
  protected $currencyExchangeRateProviderManager;

  /**
   * The form helper.
   *
   * @var \Drupal\currency\FormHelperInterface
   */
  protected $formHelper;

  /**
   * Constructs a new instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translator.
   * @param \Drupal\Core\Entity\EntityStorageInterface $currency_storage
   *   The currency storage.
   * @param \Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface $currency_exchange_rate_provider_manager
   *   The currency exchange rate provider plugin manager.
   * @param \Drupal\currency\FormHelperInterface $form_helper
   *   The form helper.
   */
  public function __construct(ConfigFactoryInterface $configFactory, TranslationInterface $string_translation, EntityStorageInterface $currency_storage, ExchangeRateProviderManagerInterface $currency_exchange_rate_provider_manager, FormHelperInterface $form_helper) {
    $this->setConfigFactory($configFactory);
    $this->currencyStorage = $currency_storage;
    $this->currencyExchangeRateProviderManager = $currency_exchange_rate_provider_manager;
    $this->formHelper = $form_helper;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation'),
      $container->get('entity_type.manager')->getStorage('currency'),
      $container->get('plugin.manager.currency.exchange_rate_provider'),
      $container->get('currency.form_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'currency_pbank_exchange_rate_provider_pbank_rates';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $currency_code = 'XXX') {
    $rate = $this->currencyExchangeRateProviderManager->createInstance('currency_pbank_rates')->load($currency_code, 'UAH');

    $form['currency_code'] = [
      '#default_value' => $currency_code,
      '#disabled' => !is_null($rate),
      '#empty_value' => '',
      '#options' => $this->getCurrencyOptions(),
      '#required' => TRUE,
      '#title' => $this->t('Currency'),
      '#type' => 'select',
    ];
    $config = $this->config('currency_pbank.exchanger')->get('pbank_rates');
    $form['rate_type'] = [
      '#default_value' => isset($config[$currency_code]) ? $config[$currency_code] : '11',
      '#options' => [
        '11' => $this->t('Cashless (conversion by cards, Privat24, deposit replenishment)'),
        '5' => $this->t('Cash (rate in Privatbank branches)'),
      ],
      '#required' => TRUE,
      '#title' => $this->t('Exchange rate type'),
      '#type' => 'radios',
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['save'] = [
      '#button_type' => 'primary',
      '#name' => 'save',
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    if (!is_null($rate)) {
      $form['actions']['delete'] = [
        '#button_type' => 'danger',
        '#limit_validation_errors' => [['currency_code'], ['rate_type']],
        '#name' => 'delete',
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\currency_pbank\Plugin\Currency\ExchangeRateProvider\PbankRates $plugin */
    $plugin = $this->currencyExchangeRateProviderManager->createInstance('currency_pbank_rates');
    $values = $form_state->getValues();
    $currency_code = $values['currency_code'];
    $rate_type = $values['rate_type'];
    $currency = $this->currencyStorage->load($currency_code);

    $triggering_element = $form_state->getTriggeringElement();
    switch ($triggering_element['#name']) {
      case 'save':
        $plugin->save($currency_code, $rate_type);
        $this->messenger()->addStatus($this->t('The exchange rate for @currency_title has been saved.', [
          '@currency_title' => $currency->label(),
        ]));
        break;

      case 'delete':
        $plugin->delete($currency_code);
        $this->messenger()->addStatus($this->t('The exchange rate for @currency_title has been deleted.', [
          '@currency_title' => $currency->label(),
        ]));
        break;
    }
    $form_state->setRedirect('currency_pbank.exchange_rate_provider.pbank_rates.overview');
  }

  /**
   * Gets only currencies that are supported by Privatbank.ua.
   *
   * @return array
   *   The list of currencies for select form element.
   */
  protected function getCurrencyOptions() {
    $allowed = explode(',', self::P24_API_AVAILABLE_CURRENCIES);
    return array_intersect_key($this->formHelper->getCurrencyOptions(), array_combine($allowed, $allowed));
  }

}
