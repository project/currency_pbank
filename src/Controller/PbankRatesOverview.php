<?php

namespace Drupal\currency_pbank\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\currency\Plugin\Currency\AmountFormatter\AmountFormatterManagerInterface;
use Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the overview of Privatbank exchange rates.
 */
class PbankRatesOverview extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The currency amount formatter manager.
   *
   * @var \Drupal\currency\Plugin\Currency\AmountFormatter\AmountFormatterManagerInterface
   */
  protected $currencyAmountFormatterManager;

  /**
   * The currency exchange rate provider manager.
   *
   * @var \Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface
   */
  protected $currencyExchangeRateProviderManager;

  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a new instance.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The translation manager.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator.
   * @param \Drupal\Core\Entity\EntityStorageInterface $currency_storage
   *   The currency storage.
   * @param \Drupal\currency\Plugin\Currency\AmountFormatter\AmountFormatterManagerInterface $currency_amount_formatter_manager
   *   The currency locale delegator.
   * @param \Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface $currency_exchange_rate_provider_manager
   *   The currency exchanger plugin manager.
   */
  public function __construct(TranslationInterface $translation_manager, UrlGeneratorInterface $url_generator, EntityStorageInterface $currency_storage, AmountFormatterManagerInterface $currency_amount_formatter_manager, ExchangeRateProviderManagerInterface $currency_exchange_rate_provider_manager) {
    $this->currencyStorage = $currency_storage;
    $this->currencyAmountFormatterManager = $currency_amount_formatter_manager;
    $this->currencyExchangeRateProviderManager = $currency_exchange_rate_provider_manager;
    $this->stringTranslation = $translation_manager;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($container->get('string_translation'), $container->get('url_generator'), $entity_type_manager->getStorage('currency'), $container->get('plugin.manager.currency.amount_formatter'), $container->get('plugin.manager.currency.exchange_rate_provider'));
  }

  /**
   * Views the configured pbank rates.
   *
   * @return array
   *   A renderable array.
   */
  public function overview() {
    /** @var \Drupal\currency_pbank\Plugin\Currency\ExchangeRateProvider\PbankRates $plugin */
    $plugin = $this->currencyExchangeRateProviderManager->createInstance('currency_pbank_rates');

    $form['rates'] = [
      '#empty' => $this->t('There are no exchange rates yet. <a href="@path">Add an exchange rate</a>.', [
        '@path' => $this->urlGenerator->generateFromRoute('currency_pbank.exchange_rate_provider.pbank_rates.add'),
      ]),
      '#header' => [
        $this->t('Currency'),
        $this->t('Sale'),
        $this->t('Buy'),
        $this->t('Operations'),
      ],
      '#type' => 'table',
    ];
    $config = $this->config('currency_pbank.exchanger')->get('pbank_rates');
    if (empty($config)) {
      return $form;
    }
    $currency_uah = $this->currencyStorage->load('UAH');
    foreach ($config as $currency_code => $rate_type) {
      $currency = $this->currencyStorage->load($currency_code);
      $row['currency']['#markup'] = $currency->label();
      foreach (['sale', 'buy'] as $type) {
        $markup = $this->currencyAmountFormatterManager->getDefaultPlugin()->formatAmount($currency_uah, $plugin->getPbankRate($rate_type, $currency_code, $type));
        $row[$type]['#markup'] = $markup;
      }
      $row['operations'] = [
        '#links' => [
          'edit' => [
            'title' => $this->t('edit'),
            'url' => Url::fromRoute('currency_pbank.exchange_rate_provider.pbank_rates.edit', [
              'currency_code' => $currency_code,
            ]),
          ],
        ],
        '#type' => 'operations',
      ];
      $form['rates'][] = $row;
    }

    return $form;
  }

}
