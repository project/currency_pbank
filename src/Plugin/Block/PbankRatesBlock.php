<?php

namespace Drupal\currency_pbank\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\currency\Plugin\Currency\AmountFormatter\AmountFormatterManagerInterface;
use Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface;
use Drupal\currency\FormHelperInterface;
use Drupal\currency_pbank\Controller\PbankRatesForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'PbankRatesBlock' block.
 *
 * @Block(
 *  id = "pbank_rates_block",
 *  admin_label = @Translation("Privatbank Rates Block"),
 * )
 */
class PbankRatesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * The currency exchange rate provider manager.
   *
   * @var \Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface
   */
  protected $currencyExchangeRateProviderManager;

  /**
   * The form helper.
   *
   * @var \Drupal\currency\FormHelperInterface
   */
  protected $formHelper;

  /**
   * The currency amount formatter manager.
   *
   * @var \Drupal\currency\Plugin\Currency\AmountFormatter\AmountFormatterManagerInterface
   */
  protected $currencyAmountFormatterManager;

  /**
   * Constructs a new PbankRatesBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $currency_storage
   *   The currency storage.
   * @param \Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderManagerInterface $currency_exchange_rate_provider_manager
   *   The currency exchange rate provider plugin manager.
   * @param \Drupal\currency\FormHelperInterface $form_helper
   *   The form helper.
   * @param \Drupal\currency\Plugin\Currency\AmountFormatter\AmountFormatterManagerInterface $currency_amount_formatter_manager
   *   The currency locale delegator.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityStorageInterface $currency_storage,
    ExchangeRateProviderManagerInterface $currency_exchange_rate_provider_manager,
    FormHelperInterface $form_helper,
    AmountFormatterManagerInterface $currency_amount_formatter_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currencyStorage = $currency_storage;
    $this->currencyExchangeRateProviderManager = $currency_exchange_rate_provider_manager;
    $this->formHelper = $form_helper;
    $this->currencyAmountFormatterManager = $currency_amount_formatter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('currency'),
      $container->get('plugin.manager.currency.exchange_rate_provider'),
      $container->get('currency.form_helper'),
      $container->get('plugin.manager.currency.amount_formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'rate_types' => [11 => '11'],
      'currencies' => ['USD' => 'USD'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['rate_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Rate Type'),
      '#options' => $this->getRateTypeOptions(),
      '#default_value' => $this->configuration['rate_types'],
    ];
    $form['currencies'] = [
      '#type' => 'select',
      '#title' => $this->t('Currencies'),
      '#options' => $this->getCurrencyOptions(),
      '#default_value' => $this->configuration['currencies'],
      '#multiple' => TRUE,
      '#size' => 3,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['rate_types'] = $form_state->getValue('rate_types');
    $this->configuration['currencies'] = $form_state->getValue('currencies');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\currency_pbank\Plugin\Currency\ExchangeRateProvider\PbankRates $plugin */
    $plugin = $this->currencyExchangeRateProviderManager->createInstance('currency_pbank_rates');
    $currency_uah = $this->currencyStorage->load('UAH');

    $build['#theme'] = 'pbank_rates';
    $build['#rate_types'] = $this->configuration['rate_types'];
    $build['#rates'] = $this->configuration['currencies'];
    foreach ($this->configuration['rate_types'] as $rate_type) {
      $build['#content'][$rate_type] = [
        '#type' => 'table',
        '#caption' => $this->getRateTypeOptions('short')[$rate_type],
        '#header' => [
          '',
          '',
          $this->t('Buy'),
          $this->t('Sell'),
        ],
      ];
      foreach ($this->configuration['currencies'] as $currency_code) {
        $currency = $this->currencyStorage->load($currency_code);
        $build['#content'][$rate_type][$currency_code]['currency']['#markup'] = $currency->id();
        $build['#content'][$rate_type][$currency_code]['currency_uah']['#markup'] = $currency_uah->id();
        foreach (['sale', 'buy'] as $type) {
          $markup = $this->currencyAmountFormatterManager->getDefaultPlugin()->formatAmount($currency_uah, $plugin->getPbankRate($rate_type, $currency_code, $type));
          $build['#content'][$rate_type][$currency_code][$type]['#markup'] = $markup;
        }
      }
    }
    $build['#cache']['max-age'] = 0;
    return $build;
  }

  /**
   * Gets only currencies that are supported by Privatbank.ua.
   *
   * @return array
   *   The list of currencies for select form element.
   */
  protected function getCurrencyOptions() {
    $allowed = explode(',', PbankRatesForm::P24_API_AVAILABLE_CURRENCIES);
    return array_intersect_key($this->formHelper->getCurrencyOptions(), array_combine($allowed, $allowed));
  }

  /**
   * Gets the list of rate type options.
   *
   * @param string $format
   *   Format (short or long).
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The list of rate type options.
   */
  protected function getRateTypeOptions($format = 'long') {
    switch ($format) {
      case 'long':
        $result = [
          '11' => $this->t('Cashless (conversion by cards, Privat24, deposit replenishment)'),
          '5' => $this->t('Cash (rate in Privatbank branches)'),
        ];
        break;

      case 'short':
        $result = [
          '11' => $this->t('Cashless'),
          '5' => $this->t('Cash'),
        ];
        break;
    }
    return $result;
  }

}
