<?php

namespace Drupal\currency_pbank\Plugin\Currency\ExchangeRateProvider;

use Commercie\CurrencyExchange\FixedExchangeRateProviderTrait;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\currency\Plugin\Currency\ExchangeRateProvider\ExchangeRateProviderInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides manually entered exchange rates.
 *
 * @CurrencyExchangeRateProvider(
 *   id = "currency_pbank_rates",
 *   label = @Translation("Privatbank exchange rates"),
 *   operations_provider = "\Drupal\currency_pbank\Plugin\Currency\ExchangeRateProvider\PbankRatesOperationsProvider"
 * )
 */
class PbankRates extends PluginBase implements ExchangeRateProviderInterface, ContainerFactoryPluginInterface {

  const P24_API_PUBINFO_URL = 'https://api.privatbank.ua/p24api/pubinfo';

  use FixedExchangeRateProviderTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A guzzle http client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Privatbank exchange rates data.
   *
   * @var array
   */
  protected $pbankRates;

  /**
   * Constructs a new class instance.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed[] $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\Client $http_client
   *   A guzzle http client instance.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactoryInterface $config_factory, Client $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadAll() {
    $pbank_rates = $this->configFactory->get('currency_pbank.exchanger')->get('pbank_rates');
    $rates = [];
    if ($pbank_rates) {
      foreach ($pbank_rates as $currency_code => $rate_type) {
        $rates[$currency_code]['UAH'] = $this->getPbankRate($rate_type, $currency_code, 'buy');
        $rates['UAH'][$currency_code] = bcdiv(1, $this->getPbankRate($rate_type, $currency_code, 'sale'), 6);
      }
    }
    return $rates;
  }

  /**
   * Saves an exchange rate.
   *
   * @param string $currency_code
   *   Currency code.
   * @param string $rate_type
   *   Rate type ('buy' or 'sale').
   *
   * @return $this
   */
  public function save($currency_code, $rate_type) {
    $config = $this->configFactory->getEditable('currency_pbank.exchanger');
    $rates_data = $config->get('pbank_rates');
    $rates_data[$currency_code] = $rate_type;
    $config->set('pbank_rates', $rates_data);
    $config->save();

    return $this;
  }

  /**
   * Deletes an exchange rate.
   *
   * @param string $currency_code
   *   Currency code.
   */
  public function delete($currency_code) {
    $config = $this->configFactory->getEditable('currency_pbank.exchanger');
    $rates = $config->get('pbank_rates');
    unset($rates[$currency_code]);
    $config->set('pbank_rates', $rates);
    $config->save();
  }

  /**
   * Gets Privatbank currency exchange rate.
   *
   * @param string $course_id
   *   Pbank course ID. Possible values: '11', '5'.
   * @param string $currency_code
   *   Currency code.
   * @param string $type
   *   Rate type: buy or sale.
   *
   * @return mixed
   *   Exchange rate if found or NULL.
   */
  public function getPbankRate($course_id = '11', $currency_code = 'XXX', $type = 'buy') {
    $pbank_currency = 'RUB' == $currency_code ? 'RUR' : $currency_code;
    if (!isset($this->pbankRates[$course_id])) {
      $this->pbankRates[$course_id] = [];
      $http_client_options = ['query' => ['exchange' => '']];
      $http_client_options['query']['json'] = '';
      $http_client_options['query']['coursid'] = $course_id;

      $response = $this->httpClient->request('GET', self::P24_API_PUBINFO_URL, $http_client_options);
      if (200 == $response->getStatusCode()) {
        $contents = Json::decode($response->getBody()->getContents());
        $this->pbankRates[$course_id] = array_combine(array_column($contents, 'ccy'), $contents);
      }
    }
    return isset($this->pbankRates[$course_id][$pbank_currency][$type]) ? $this->pbankRates[$course_id][$pbank_currency][$type] : NULL;
  }

}
